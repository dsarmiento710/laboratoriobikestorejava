FROM openjdk:8 as build
WORKDIR /api
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE
ADD ./target/Bike-0.0.1-SNAPSHOT.jar backend-server-bikestore.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","backend-server-bikestore.jar"]

